let state = {
  color: "#333",
};
window.addEventListener("load", (e) => {
  const canvas = document.getElementById("canvas");
  const ctx = canvas.getContext("2d");

  const welcomeMessage = document.getElementById("welcome-box");
  setTimeout(() => {
    welcomeMessage.style.display = "none";
  }, 5000);

  //for color changings

  //resize canvas
  canvas.height = window.innerHeight - 12;
  canvas.width = window.innerWidth - 6;

  ctx.beginPath();

  let painting = false;

  //events listeners
  function startPosition(e) {
    painting = true;
    document.getElementById("toggle").classList.remove("show-option");
    draw(e);
    welcomeMessage.style.display = "none";
  }

  function finishPosition() {
    painting = false;
    ctx.beginPath();
  }

  function draw(e) {
    if (!painting) {
      return;
    } else {
      ctx.lineWidth = 5; //paint brush width
      ctx.lineCap = "round";
      ctx.lineTo(e.pageX, e.pageY); //get mouse position
      ctx.strokeStyle = state.color; //paint color
      ctx.stroke(); //ask to draw
      ctx.beginPath();
      ctx.moveTo(e.clientX, e.clientY);
      paintColor();
    }
  }

  canvas.addEventListener("mousedown", startPosition);
  canvas.addEventListener("mouseup", finishPosition);

  canvas.addEventListener("mousemove", draw);
});

function paintColor() {
  state.color = document.getElementById("select-color").value;
  return state;
}

document.getElementById("btn").addEventListener("click", () => {
  document.getElementById("toggle").classList.toggle("show-option");
});

window.addEventListener("resize", (e) => {
  //resizing the canvas on window resize
  canvas.height = window.innerHeight - 12;
  canvas.width = window.innerWidth - 6;
});
